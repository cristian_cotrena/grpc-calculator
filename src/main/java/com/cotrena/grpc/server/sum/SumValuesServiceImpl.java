package com.cotrena.grpc.server.sum;

import com.cotrena.grpc.constants.error.MessageErrors;
import com.cotrena.grpc.validation.sum.SumValuesValidation;
import com.proto.sum_values.SumValuesRequest;
import com.proto.sum_values.SumValuesResponse;
import com.proto.sum_values.SumValuesServiceGrpc;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.math.BigDecimal;
import java.math.MathContext;

public class SumValuesServiceImpl extends SumValuesServiceGrpc.SumValuesServiceImplBase {
    @Override
    public void execute(SumValuesRequest request, StreamObserver<SumValuesResponse> responseObserver) {
        SumValuesServiceImpl service = new SumValuesServiceImpl();
        BigDecimal result = service.toBigDecimal(0.0);

        StatusRuntimeException validation = new SumValuesValidation().validate(request);
        if (validation != null) {
            responseObserver.onError(validation);
            return;
        }

        for (double item : request.getValuesList()) {
            result = result.add(service.toBigDecimal(item), MathContext.DECIMAL32);
        }

        responseObserver.onNext(SumValuesResponse.newBuilder().setResult(result.doubleValue()).build());
        responseObserver.onCompleted();
    }

    private BigDecimal toBigDecimal(Double value) {
        return new BigDecimal(value).setScale(4);
    }
}
