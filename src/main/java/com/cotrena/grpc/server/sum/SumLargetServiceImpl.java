package com.cotrena.grpc.server.sum;

import com.proto.sum_larget.SumTheLargestRequest;
import com.proto.sum_larget.SumTheLargestResponse;
import com.proto.sum_larget.SumTheLargestServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.math.BigDecimal;
import java.math.MathContext;

public class SumLargetServiceImpl extends SumTheLargestServiceGrpc.SumTheLargestServiceImplBase {
    @Override
    public StreamObserver<SumTheLargestRequest> execute(StreamObserver<SumTheLargestResponse> responseObserver) {
        StreamObserver<SumTheLargestRequest> observer = new StreamObserver<> () {
            BigDecimal result;
            Double largest;
            @Override
            public void onNext(SumTheLargestRequest value) {
                if (result == null) {
                    result = toBigDecimal(0.0);
                    largest = 0.0;
                }
                if (largest < value.getValue()) {
                    largest = value.getValue();
                    result = result.add(toBigDecimal(value.getValue()), MathContext.DECIMAL32);
                }
            }

            @Override
            public void onError(Throwable t) {
                // TODO - ERROR . . .
            }

            @Override
            public void onCompleted() {
                responseObserver.onNext(
                        SumTheLargestResponse.newBuilder()
                                .setResult(result.doubleValue())
                                .build());
                responseObserver.onCompleted();
            }
        };

        return observer;
    }

    private BigDecimal toBigDecimal(Double value) {
        return new BigDecimal(value).setScale(4);
    }
}
