package com.cotrena.grpc.server;

import com.cotrena.grpc.server.squareRoot.SquareRootServiceImpl;
import com.cotrena.grpc.server.sum.SumLargetServiceImpl;
import com.cotrena.grpc.server.sum.SumValuesServiceImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.File;
import java.io.IOException;

public class MainServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(50051)
                .addService(new SumLargetServiceImpl())
                .addService(new SumValuesServiceImpl())
                .addService(new SquareRootServiceImpl())
                .useTransportSecurity(
                        new File("ssl/server.crt"),
                        new File("ssl/server.pem")
                )
                .build();

        server.start();

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            System.out.println("Receive Shutdown Request");
            server.shutdown();
            System.out.println("Successfully stopped the server");
        }));

        server.awaitTermination();
    }
}
