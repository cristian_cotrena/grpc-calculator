package com.cotrena.grpc.server.squareRoot;

import com.cotrena.grpc.constants.error.MessageErrors;
import com.cotrena.grpc.validation.squareRoot.SquareRootValidation;
import com.proto.sum_larget.SquareRootRequest;
import com.proto.sum_larget.SquareRootResponse;
import com.proto.sum_larget.SquareRootServiceGrpc;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.math.BigDecimal;
import java.math.MathContext;

public class SquareRootServiceImpl extends SquareRootServiceGrpc.SquareRootServiceImplBase {
    @Override
    public void execute(SquareRootRequest request, StreamObserver<SquareRootResponse> responseObserver) {
        StatusRuntimeException validationResult = new SquareRootValidation().validate(request);

        if (validationResult != null) {
            responseObserver.onError(validationResult);
            return;
        }

        BigDecimal result = toBigDecimal(request.getValue().getValue() , 4);
        result = result.sqrt(MathContext.DECIMAL32);

        SquareRootResponse response = SquareRootResponse.newBuilder()
                .setResult(result.doubleValue())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    private BigDecimal toBigDecimal(Integer value, Integer scale) {
        return new BigDecimal(value).setScale(scale != null ? scale : 4);
    }
}
