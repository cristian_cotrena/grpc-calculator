package com.cotrena.grpc.base;

import io.grpc.*;

import java.io.File;
import java.io.IOException;

public class ClientBase {
    public static final ManagedChannel MANAGED_CHANNEL = createChannel();
    public static final ManagedChannel SECURITY_CHANNEL = createSecurityChannel();

    private static ManagedChannel createChannel() {
        return ManagedChannelBuilder.forAddress("localhost", 50051)
                .usePlaintext()
                .build();
    }

    private static ManagedChannel createSecurityChannel() {
        ChannelCredentials creds = null;
        try {
            creds = TlsChannelCredentials.newBuilder()
                    .trustManager(new File("ssl/ca.crt")) // "server.pem"
                    .build();
            return Grpc.newChannelBuilder("localhost:50051", creds)
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
