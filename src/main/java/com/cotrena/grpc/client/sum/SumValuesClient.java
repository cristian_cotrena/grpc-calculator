package com.cotrena.grpc.client.sum;

import com.cotrena.grpc.base.ClientBase;
import com.proto.sum_values.SumValuesRequest;
import com.proto.sum_values.SumValuesResponse;
import com.proto.sum_values.SumValuesServiceGrpc;

import java.util.Arrays;

public class SumValuesClient extends ClientBase {
    public double execute(){
        SumValuesServiceGrpc.SumValuesServiceBlockingStub sumClient = SumValuesServiceGrpc.newBlockingStub(SECURITY_CHANNEL);

        // SEND MOCK VALUES . . .
        SumValuesRequest request = SumValuesRequest.newBuilder()
                .addAllValues(Arrays.asList(250.0, 300.5, 200.5, 249.0))
                .build();

        SumValuesResponse response = sumClient.execute(request);
        return response.getResult();
    }
}
