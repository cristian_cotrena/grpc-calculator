package com.cotrena.grpc.client.sum;

import com.cotrena.grpc.base.ClientBase;
import com.proto.sum_larget.SumTheLargestRequest;
import com.proto.sum_larget.SumTheLargestResponse;
import com.proto.sum_larget.SumTheLargestServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class SumTheLargestClient  extends ClientBase {
    double result;
    public double execute() {
        SumTheLargestServiceGrpc.SumTheLargestServiceStub sumClient = SumTheLargestServiceGrpc.newStub(SECURITY_CHANNEL);
        CountDownLatch latch = new CountDownLatch(1);
        StreamObserver<SumTheLargestRequest> requestObserver = sumClient.execute(new StreamObserver<>() {

            @Override
            public void onNext(SumTheLargestResponse value) {
                result = value.getResult();
            }

            @Override
            public void onError(Throwable t) {}

            @Override
            public void onCompleted() {
                latch.countDown();
            }
        });

        // SEND MOCK VALUES . . .
        requestObserver.onNext(SumTheLargestRequest.newBuilder().setValue(300).build());
        requestObserver.onNext(SumTheLargestRequest.newBuilder().setValue(400).build());
        requestObserver.onNext(SumTheLargestRequest.newBuilder().setValue(500).build());
        requestObserver.onNext(SumTheLargestRequest.newBuilder().setValue(800).build());
        requestObserver.onNext(SumTheLargestRequest.newBuilder().setValue(8000).build());

        requestObserver.onCompleted();

        try {
            latch.await(10L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("CONCLUIDO: " + result);
            return result;
        }
    }
}
