package com.cotrena.grpc.client.squareRoot;

import com.cotrena.grpc.base.ClientBase;
import com.google.protobuf.Int32Value;
import com.proto.sum_larget.SquareRootRequest;
import com.proto.sum_larget.SquareRootResponse;
import com.proto.sum_larget.SquareRootServiceGrpc;

public class SquareRootClient extends ClientBase {
    public double execute() {
        SquareRootServiceGrpc.SquareRootServiceBlockingStub client = SquareRootServiceGrpc.newBlockingStub(SECURITY_CHANNEL);
        SquareRootRequest request = SquareRootRequest.newBuilder()
                .setValue(Int32Value.of(9))
                .build();
        SquareRootResponse result = client.execute(request);
        return result.getResult();
    }
}
