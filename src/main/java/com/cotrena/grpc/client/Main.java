package com.cotrena.grpc.client;

import com.cotrena.grpc.client.squareRoot.SquareRootClient;
import com.cotrena.grpc.client.sum.SumTheLargestClient;
import com.cotrena.grpc.client.sum.SumValuesClient;

public class Main {
    public static void main(String[] args) {
        SumValuesClient sumValuesClient = new SumValuesClient();
        SumTheLargestClient sumTheLargestClient = new SumTheLargestClient();
        SquareRootClient squareRootClient = new SquareRootClient();

        double resultSum = sumValuesClient.execute();
        double resultSumLargest = sumTheLargestClient.execute();
        double squareRoot = squareRootClient.execute();

        System.out.println("RESULT SUM: " + resultSum);
        System.out.println("RESULT SUM LARGEST: " + resultSumLargest);
        System.out.println("SQUARE ROOT: " + squareRoot);
    }
}
