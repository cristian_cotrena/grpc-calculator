package com.cotrena.grpc.validation.squareRoot;

import com.cotrena.grpc.constants.error.MessageErrors;
import com.proto.sum_larget.SquareRootRequest;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;

public class SquareRootValidation {
    public StatusRuntimeException validate (SquareRootRequest request) {

        if (!request.getValue().isInitialized()) {
            return  getError(
                    Status.INVALID_ARGUMENT,
                    MessageErrors.REQUIRED_VALUE
            );
        }

        if (request.getValue().getValue() < 1) {
           return  getError(
                    Status.INVALID_ARGUMENT,
                    MessageErrors.MORE_THAN_ZERO
            );
        }

        return null;
    }

    private StatusRuntimeException getError(Status status, String error) {
        return status.withDescription(error).asRuntimeException();
    }
}
