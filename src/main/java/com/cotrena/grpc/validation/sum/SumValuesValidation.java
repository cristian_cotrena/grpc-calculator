package com.cotrena.grpc.validation.sum;

import com.cotrena.grpc.constants.error.MessageErrors;
import com.proto.sum_values.SumValuesRequest;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;

public class SumValuesValidation {
    public StatusRuntimeException validate (SumValuesRequest request) {
        if (request.getValuesList().size() < 2) {
            return  getError(
                    Status.INVALID_ARGUMENT,
                    MessageErrors.INSUFFICIENT_VALUES
            );
        }

        return null;
    }

    private StatusRuntimeException getError(Status status, String error) {
        return status.withDescription(error).asRuntimeException();
    }
}
