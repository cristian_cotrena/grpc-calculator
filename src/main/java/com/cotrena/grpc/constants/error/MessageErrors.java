package com.cotrena.grpc.constants.error;

public @interface MessageErrors {
    //Sum
    String INSUFFICIENT_VALUES = "Informe pelo menos dois valores.";
    //SquareRoot
    String MORE_THAN_ZERO = "O valor informado deve ser maior que zero.";
    String REQUIRED_VALUE = "value: Campo obrigatório.";
}
